﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.Diagnostics;

namespace ServicioGestionDocumentos.Gestion.Documentos.Bussines
{
    public static class Documentos
    {
        public static void ManagementFiles()
        {
            bool ok = false;
            string nombreArchivo = "";
            string rutaDestinoNueva = "";
            string rutaArchivoDestino = "";
            string rutaArchivoOrigen = "";

            try
            {
                // recorrer todos los archivos de la ruta origen
                string rutaOrigen = ConfigurationSettings.AppSettings["rutaOrigen"].ToString();
                string rutaDestino = ConfigurationSettings.AppSettings["rutaDestino"].ToString();

                var archivos = Directory.GetFiles(rutaOrigen);

                foreach (var archivo in archivos)
                {
                    string extesion = Path.GetExtension(archivo).Trim('.');

                    nombreArchivo = Path.GetFileName(archivo);

                    if (extesion == "exe" || extesion == "msi")
                    {
                        rutaDestinoNueva = Path.Combine(rutaDestino, "Programas");
                    }
                    else
                    {
                        rutaDestinoNueva = Path.Combine(rutaDestino, extesion);
                    }

                    rutaArchivoDestino = Path.Combine(rutaDestinoNueva, nombreArchivo);
                    rutaArchivoOrigen = Path.Combine(rutaOrigen, nombreArchivo);

                    crearDirectorioNuevo(rutaDestinoNueva);
                    gestionarArchivo(rutaArchivoOrigen, rutaArchivoDestino);
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Metodo ManagementFiles", $"Error: {ex.Message} StackTrace: {ex.StackTrace}", EventLogEntryType.Error);
            }
        }

        private static void crearDirectorioNuevo(string rutaDestinoNueva)
        {
            if (!Directory.Exists(rutaDestinoNueva))
            {
                // crear directorio segun la extecion del los archivos
                Directory.CreateDirectory(rutaDestinoNueva);
            }
        }

        private static void gestionarArchivo(string rutaArchivoOrigen, string rutaArchivoDestino)
        {
            // mover los archivos a sus repectivas carpetas
            if (File.Exists(rutaArchivoOrigen))
            {
                // valido si el archivo existe en el aruta destino eliminelo
                if (File.Exists(rutaArchivoDestino))
                {
                    File.Delete(rutaArchivoDestino);
                }
                // copio el archivo y luego lo elimino de la ruta origen
                File.Copy(rutaArchivoOrigen, rutaArchivoDestino);
                File.Delete(rutaArchivoOrigen);
            }
        }
    }
}
