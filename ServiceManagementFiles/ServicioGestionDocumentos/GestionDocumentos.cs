﻿using ServicioGestionDocumentos.Gestion.Documentos.Bussines;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace ServicioGestionDocumentos
{
    partial class GestionDocumentos : ServiceBase
    {
        bool Ok = false;
        public GestionDocumentos()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            tmLapso.Start();
        }

        protected override void OnStop()
        {
            tmLapso.Stop();
        }

        private void tmLapso_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (Ok)
            {
                return;
            }
            else 
            {
                Ok = true;
                Documentos.ManagementFiles();
                Ok = false;
            }
        }
    }
}
