﻿
namespace ServicioGestionDocumentos
{
    partial class GestionDocumentos
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tmLapso = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.tmLapso)).BeginInit();
            // 
            // tmLapso
            // 
            this.tmLapso.Enabled = true;
            this.tmLapso.Interval = 300000D;
            this.tmLapso.Elapsed += new System.Timers.ElapsedEventHandler(this.tmLapso_Elapsed);
            // 
            // GestionDocumentos
            // 
            this.ServiceName = "GestionDocumentos";
            ((System.ComponentModel.ISupportInitialize)(this.tmLapso)).EndInit();

        }

        #endregion

        private System.Timers.Timer tmLapso;
    }
}
